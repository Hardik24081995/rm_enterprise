package com.rm.enterprise.utils.picker

enum class Sources {
    CAMERA, GALLERY, DOCUMENTS, CHOOSER
}