package com.rm.enterprise.activity

import android.os.Bundle
import com.rm.enterprise.R
import com.rm.enterprise.extention.getValue
import com.rm.enterprise.extention.visible
import com.rm.enterprise.modal.SalaryDataItem
import com.rm.enterprise.utils.Constant
import kotlinx.android.synthetic.main.activity_salary_detail.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*
import java.text.DecimalFormat

class SalaryDetailActivity : BaseActivity() {
    var salaryDataIteam: SalaryDataItem? = null
    var df: DecimalFormat = DecimalFormat("##.##")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_salary_detail)

        if (intent.hasExtra(Constant.DATA)) {
            salaryDataIteam = intent.getSerializableExtra(Constant.DATA) as SalaryDataItem
        }


        txtMonthSalary.text =
            getString(R.string.RS) + " " + df.format(salaryDataIteam?.salary?.toBigDecimal())
        txtPayAmount.text = getString(R.string.RS) + " " + df.format(
            (salaryDataIteam?.present?.toBigDecimal()
                ?.times(salaryDataIteam?.rate?.toBigDecimal()!!))
        )

        txtTotalPresent.text = salaryDataIteam?.present + " Presents"

        txtPresentAmount.text =
            getString(R.string.RS) + " " + df.format((salaryDataIteam?.rate?.toDouble()))


        txtPenaltyAmount.text =
            getString(R.string.RS) + " " + df.format((salaryDataIteam?.penalty?.toDouble()))



        txtOverTimeAmount.text =
            getString(R.string.RS) + " " + df.format((salaryDataIteam?.rate?.toDouble())?.times((salaryDataIteam?.halfOverTime?.toDouble()!!) + (salaryDataIteam?.fullOverTime?.toDouble()!!)))

        textviewSemiBold2.text = salaryDataIteam?.salaryDate


        txtAdvanceAmount.text = getString(R.string.RS) + " " + df.format(
            (salaryDataIteam?.payAmount?.toBigDecimal()!! - ((salaryDataIteam?.present?.toBigDecimal()!! + salaryDataIteam?.halfDay?.toBigDecimal()!! + salaryDataIteam?.halfOverTime?.toBigDecimal()!! + salaryDataIteam?.fullOverTime?.toBigDecimal()!!)?.times(
                salaryDataIteam?.rate?.toBigDecimal()!!
            )!!))
        )
        txtAdvanceAmount.text = txtAdvanceAmount.getValue().replace("-", "")

        imgBack.visible()
        imgBack.setOnClickListener {
            finish()
        }
        txtTitle.text =
            salaryDataIteam?.firstName.toString() + " " + salaryDataIteam?.lastName.toString()
    }

}