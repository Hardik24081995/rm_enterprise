package com.rm.enterprise.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.RadioButton
import com.rm.enterprise.R
import com.rm.enterprise.extention.*
import com.rm.enterprise.modal.CommonAddModal
import com.rm.enterprise.modal.EmployeeDataItem
import com.rm.enterprise.modal.GetUserSalaryDetail
import com.rm.enterprise.network.CallbackObserver
import com.rm.enterprise.network.Networking
import com.rm.enterprise.network.addTo
import com.rm.enterprise.utils.Constant
import com.rm.enterprise.utils.SessionManager
import com.rm.enterprise.utils.TimeStamp
import com.rm.enterprise.utils.TimeStamp.formatDateFromString
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_salary.*
import kotlinx.android.synthetic.main.activity_add_salary.btnSubmit
import kotlinx.android.synthetic.main.activity_add_salary.edtEndDate
import kotlinx.android.synthetic.main.activity_add_salary.edtStartDate
import kotlinx.android.synthetic.main.activity_add_salary.rg
import kotlinx.android.synthetic.main.activity_add_salary.root
import kotlinx.android.synthetic.main.dialog_accept_reason.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.txtTitle
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat


class AddSalaryActivity : BaseActivity() {

    var employeeDataItem: EmployeeDataItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_add_salary)
        txtTitle.text = "Salary"
        imgBack.visible()
        if (intent.hasExtra(Constant.DATA)) {
            employeeDataItem = intent.getSerializableExtra(Constant.DATA) as EmployeeDataItem
        }

        imgBack.setOnClickListener {
            finish()
        }

        edtStartDate.setText(getCurrentDate())


        edtStartDate.setText(TimeStamp.getStartDateRange())
        edtEndDate.setText(getCurrentDate())

        edtStartDate.setOnClickListener { showDateTimePicker(this@AddSalaryActivity, edtStartDate) }

        edtEndDate.setOnClickListener { showDateTimePicker(this@AddSalaryActivity, edtEndDate) }


        edtEndDate.setOnClickListener {
            showNextFromStartDateTimePicker(
                this@AddSalaryActivity,
                edtEndDate,
                edtStartDate.getValue()
            )
        }

        edtStartDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                edtEndDate.setText(getCurrentDate())
                GetUserSalaryDetail()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        edtEndDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                GetUserSalaryDetail()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })


        GetUserSalaryDetail()

        btnSubmit.setOnClickListener {
            if (edtPayment.getValue().isEmpty()) {
                root.showSnackBar("Please enter Payable amount")
                edtPayment.requestFocus()
            } else {
                AddSalary()
            }
        }


        rg.setOnCheckedChangeListener({ group, checkedId ->
            val radio: RadioButton = findViewById(checkedId)
            if (radio.text.equals(getString(R.string.received))) {
                til11.visible()
            } else if (radio.text.equals(getString(R.string.paid))) {
                til11.visible()
            } else {
                til11.invisible()
            }
        })

    }

    fun GetUserSalaryDetail() {
        showProgressbar()
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("UserID", employeeDataItem?.userID)
            jsonBody.put("StartDate", formatDateFromString(edtStartDate.getValue()))
            jsonBody.put("EndDate", formatDateFromString(edtEndDate.getValue()))
            jsonBody.put("CityID", session.getDataByKey(SessionManager.KEY_CITY_ID))
            result = Networking.setParentJsonData(
                Constant.METHOD_GET_SALARY_DATA,
                jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }


        Networking
            .with(this@AddSalaryActivity)
            .getServices()
            .getUserSalaryList(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<GetUserSalaryDetail>() {
                override fun onSuccess(response: GetUserSalaryDetail) {
                    hideProgressbar()
                    if (response.error == 200) {
                        var df = DecimalFormat("##.##")
                        edtSalary.setText(response.data.get(0).salary)
                        edtPerDaySalary.setText(df.format((response.data.get(0).salary?.toFloat()!! / 30)))
                        edtPresent.setText(response.data.get(0).presentCount)
                        edtAbsent.setText(response.data.get(0).absentCount)
                        edtHalfDay.setText(response.data.get(0).halfDayCount)
                        edtHalfDayOt.setText(response.data.get(0).halfOverTime)
                        edtFullDay.setText(response.data.get(0).fullOverTime)
                        edtAdvanceAmount.setText(response.data.get(0).advance)
                        edtPanaltyAmount.setText(response.data.get(0).penalty)
                        penalty_amount_title.text =
                            "${getString(R.string.panalty_amount)} : ${getString(R.string.RS)} ${
                                response.data.get(0).penalty
                            }"

                        adavance_amount_title.text =
                            "${getString(R.string.advance_amount)} : ${getString(R.string.RS)} ${
                                response.data.get(0).advance
                            }  (${response.data.get(0).advanceType})"


                        var tempTotalDay: Float =
                            response.data.get(0).presentCount?.toFloat()!! + response.data.get(
                                0
                            ).halfDayCount?.toFloat()!! + response.data.get(0).halfOverTime?.toFloat()!! + response.data.get(
                                0
                            ).fullOverTime?.toFloat()!!

                        edtPayment.setText(
                            df.format(
                                tempTotalDay * edtPerDaySalary.getValue().toFloat()
                            )
                        )


                    } else {
                        showAlert(response.message.toString())
                    }


                }

                override fun onFailed(code: Int, message: String) {

                    hideProgressbar()

                     // showAlert(message)
                    showAlert(getString(R.string.show_server_error))

                }

            }).addTo(autoDisposable)
    }

    fun AddSalary() {
        showProgressbar()
        var result = ""
        try {

            var payAmount: Float = 0f


            var adavance: Int = 0
            var Penalty: Int = 0

            if (!edtAdvanceAmount.getValue().equals("")) {
                adavance = edtAdvanceAmount.getValue().toInt()
            }

            if (!edtPanaltyAmount.getValue().equals("")) {
                Penalty = edtPanaltyAmount.getValue().toInt()
            }

            if (rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId())) == 0) {
                payAmount = edtPayment.getValue().toFloat() - adavance - Penalty
            } else if (rg.indexOfChild(findViewById(rg.getCheckedRadioButtonId())) == 1) {
                payAmount = edtPayment.getValue().toFloat() + adavance - Penalty
            } else {
                payAmount = edtPayment.getValue().toFloat() - Penalty
            }


            val rbType = findViewById<View>(rg.getCheckedRadioButtonId()) as? RadioButton


            val jsonBody = JSONObject()
            jsonBody.put("UserID", session.user.data?.userID)
            jsonBody.put("EmployeeID", employeeDataItem?.userID.toString())
            jsonBody.put("SalaryDate", formatDateFromString(getCurrentDate()))
            jsonBody.put("StartDate", formatDateFromString(edtStartDate.getValue()))
            jsonBody.put("EndDate", formatDateFromString(edtEndDate.getValue()))
            jsonBody.put("Present", edtPresent.getValue())
            jsonBody.put("Absent", edtAbsent.getValue())
            jsonBody.put("HalfDay", edtHalfDay.getValue())
            jsonBody.put("HalfOverTime", edtHalfDayOt.getValue())
            jsonBody.put("FullOverTime", edtFullDay.getValue())
            jsonBody.put("Rate", edtPerDaySalary.getValue())
            jsonBody.put("Penalty", edtPanaltyAmount.getValue())
            jsonBody.put("PayAmount", payAmount)
            jsonBody.put("Amount", edtAdvanceAmount.getValue())
            jsonBody.put("Type", rbType?.text.toString())


            result = Networking.setParentJsonData(
                Constant.METHOD_ADD_SALARY,
                jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }


        Networking
            .with(this@AddSalaryActivity)
            .getServices()
            .addSalary(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<CommonAddModal>() {
                override fun onSuccess(response: CommonAddModal) {
                    hideProgressbar()
                    if (response.error == 200) {

                        finish()
                    } else {
                        showAlert(response.message.toString())
                    }

                }

                override fun onFailed(code: Int, message: String) {

                    hideProgressbar()

                     // showAlert(message)
                    showAlert(getString(R.string.show_server_error))

                }

            }).addTo(autoDisposable)
    }
}








