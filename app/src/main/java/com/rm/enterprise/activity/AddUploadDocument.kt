package com.rm.enterprise.activity


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.bumptech.glide.Glide
import com.rm.enterprise.R
import com.rm.enterprise.dialog.ImagePickerBottomSheetDialog
import com.rm.enterprise.extention.*
import com.rm.enterprise.modal.CommonAddModal
import com.rm.enterprise.modal.SiteListItem
import com.rm.enterprise.network.CallbackObserver
import com.rm.enterprise.network.Networking
import com.rm.enterprise.utils.Constant
import com.rm.enterprise.utils.Logger
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_add_document.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddUploadDocument : BaseActivity() {

    var resultUri: Uri? = null
    var resultUriProfile: Uri? = null
    var siteListItem: SiteListItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_add_document)
        imgBack.visible()
        imgBack.setOnClickListener {
            finish()
        }

        if (intent.hasExtra(Constant.DATA)) {
            siteListItem = intent.getSerializableExtra(Constant.DATA) as SiteListItem
        }
        txtTitle.text = "Upload Document"




        imgDocument.setOnClickListener {

            val dialog = ImagePickerBottomSheetDialog
                .newInstance(
                    this,
                    object : ImagePickerBottomSheetDialog.OnModeSelected {
                        override fun onMediaPicked(uri: Uri) {
                            val destinationUri = Uri.fromFile(
                                File(
                                    cacheDir,
                                    "IMG_" + System.currentTimeMillis()
                                )
                            )
                            UCrop.of(uri, destinationUri)
                                .withAspectRatio(1f, 1f)
                                .start(this@AddUploadDocument, 1)
                        }

                        override fun onError(message: String) {
                             // showAlert(message)
                    showAlert(getString(R.string.show_server_error))
                        }
                    })
            dialog.show(supportFragmentManager, "ImagePicker")
        }




        btnSubmit.setOnClickListener {
            validation()
        }

    }


    fun validation() {
        when {
            resultUriProfile == null -> {
                root.showSnackBar("Upload  Image")
            }

            edtTitle.isEmpty() -> {
                root.showSnackBar("Enter Title")
                edtTitle.requestFocus()
            }

            else -> {
                AddDocument()
            }

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when {
            requestCode == UCrop.REQUEST_CROP -> {
                if (resultCode == Activity.RESULT_OK) {
                    resultUri = UCrop.getOutput(data!!)
                }
            }
            requestCode == 1 -> {
                resultUriProfile = UCrop.getOutput(data!!)
                Glide.with(this)
                    .load(resultUriProfile)
                    .apply(
                        com.bumptech.glide.request.RequestOptions().centerCrop()
                            .placeholder(com.rm.enterprise.R.drawable.ic_profile)
                    )
                    .into(imgDocument)

            }

        }
    }

    fun AddDocument() {
        showProgressbar()

        val requestFile: RequestBody =
            RequestBody.create("image/*".toMediaTypeOrNull(), File(resultUriProfile?.path))
        val body: MultipartBody.Part = MultipartBody.Part.createFormData(
            "ImageData",
            File(resultUriProfile?.path).name + ".jpg",
            requestFile
        )


        val methodName: RequestBody =
            RequestBody.create(
                "text/plain".toMediaTypeOrNull(),
                Constant.METHOD_ADD_CUSTOMER_SITE_DOCUMENT
            )

        Networking
            .with(this)
            .getServices()
            .AddCustomerSiteDocument(
                body,
                methodName,
                RequestBody.create(
                    "text/plain".toMediaTypeOrNull(),
                    session.user.data?.userID.toString()
                ),
                RequestBody.create("text/plain".toMediaTypeOrNull(), edtTitle.getValue()),
                RequestBody.create(
                    "text/plain".toMediaTypeOrNull(),
                    siteListItem?.sitesID.toString()
                )

            )//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CallbackObserver<CommonAddModal>() {
                override fun onSuccess(response: CommonAddModal) {
                    hideProgressbar()
                    if (response.error == 200) {
                        root.showSnackBar(response.message.toString())
                        finish()
                    } else {
                        showAlert(response.message.toString())
                    }
                }

                override fun onFailed(code: Int, message: String) {
                    Logger.d("data", message.toString())
                }

            })


    }


}