package com.rm.enterprise.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rm.enterprise.R
import com.rm.enterprise.extention.getRandomMaterialColor
import com.rm.enterprise.extention.invisible
import com.rm.enterprise.extention.openPDF
import com.rm.enterprise.extention.visible
import com.rm.enterprise.modal.InvoiceDataItem
import com.rm.enterprise.utils.Constant
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_invoice.*


class InvoicePaidAdapter(
    private val mContext: Context,
    var list: MutableList<InvoiceDataItem> = mutableListOf(),
    var isPaid: Boolean = false,
    var flag: String,
    private val listener: InvoicePaidAdapter.OnItemSelected
) : RecyclerView.Adapter<InvoicePaidAdapter.ItemHolder>() {
    val MAX_LINES = 1
    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.row_invoice,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, isPaid, flag)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: InvoiceDataItem)
    }

    class ItemHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {


        fun bindData(
            context: Context,
            data: InvoiceDataItem,
            listener: InvoicePaidAdapter.OnItemSelected,
            isPaid: Boolean,
            flag: String,
        ) {

            txtName.text = data.siteUserFrindlyName
            txtInvoiceno.text = data.invoiceNo
            txtEstimateno.text = data.estimateNo
            txtInvoiceDate.text = data.invoiceDate
            txtNotes.text = data.notes
            txtTerms.text = data.terms
            txtSubtotal.text = data.subTotal
            txtTotal.text = data.totalAmount


            if (data.cGST.equals(""))
                txtCgst.text = "N/A"
            else
                txtCgst.text = data.cGST

            if (data.sGST.equals(""))
                txtSgst.text = "N/A"
            else
                txtSgst.text = data.sGST


            if (data.iGST.equals(""))
                txtIgst.text = "N/A"
            else
                txtIgst.text = data.iGST




            if (!flag.equals("Paid")) {
                crd_remaining.visible()
                texRemainingPayment.text = data.remainingPayment
                texRemainingGST.text = data.remainingGSTPayment
            } else {
                crd_remaining.invisible()
            }

            if (isPaid) {
                txtNotes.invisible()
                btnPay.invisible()
            } else {
                txtNotes.visible()
            }
            txtNotes.setOnClickListener {
                if (txtNotes.maxLines == 1) txtNotes.setMaxLines(100) else txtNotes.setMaxLines(
                    1
                )
            }

            imgProfile.setImageResource(R.drawable.bg_circle)
            imgProfile.setColorFilter(getRandomMaterialColor("400", context))
            txtIcon.text = data.siteUserFrindlyName.toString().substring(0, 1)
            txtIcon.visible()
            btnPay.setOnClickListener { listener.onItemSelect(adapterPosition, data) }

            imgPrint.setOnClickListener {
                openPDF(
                    Constant.PDF_INVOICE_URL + data.document,
                    context
                )
            }
        }


    }


}