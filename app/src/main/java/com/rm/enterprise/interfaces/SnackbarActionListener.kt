package com.rm.enterprise.interfaces

/**
 * Created by HARDIK
 */

interface SnackbarActionListener {
    fun onAction()
}
