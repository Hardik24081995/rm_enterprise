package com.rm.enterprise.activity

import android.os.Bundle
import com.rm.enterprise.R
import com.rm.enterprise.extention.*
import com.rm.enterprise.modal.ChangePasswordModal
import com.rm.enterprise.network.CallbackObserver
import com.rm.enterprise.network.Networking
import com.rm.enterprise.network.addTo
import com.rm.enterprise.utils.Constant
import com.rm.enterprise.utils.SessionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*
import org.json.JSONException
import org.json.JSONObject


class ChangePasswordActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        txtTitle.setText(getString(R.string.change_password))

        imgBack.setOnClickListener {
            finish()
        }
        btnSubmit.setOnClickListener { validation() }

    }

    fun validation() {
        when {
            edtOldPassword.isEmpty() -> {
                root.showSnackBar("Enter OLD Password")
                edtOldPassword.requestFocus()
            }
            edtOldPassword.getValue().length < 6 -> {
                root.showSnackBar("Enter Minimum six character")
                edtOldPassword.requestFocus()
            }
            edtNewPassword.getValue().equals(edtOldPassword.getValue()) -> {
                edtConfirmPassword.requestFocus()
                root.showSnackBar("OLD Password and New Password can not be same")
            }
            edtNewPassword.isEmpty() -> {
                root.showSnackBar("Enter New Password")
                edtNewPassword.requestFocus()
            }
            edtNewPassword.getValue().length < 6 -> {
                edtNewPassword.requestFocus()
                root.showSnackBar("Enter Minimum six character")
            }
            !edtNewPassword.getValue().equals(edtConfirmPassword.getValue()) -> {
                edtConfirmPassword.requestFocus()
                root.showSnackBar("New Password and Confirm Password not matched")
            }

            else -> {
                changePWD()
            }

        }
    }

    fun changePWD() {
        var result = ""
        showProgressbar()
        try {
            val jsonBody = JSONObject()
            jsonBody.put("UserID", session.user.data?.userID)
            jsonBody.put("OldPassword", edtOldPassword.getValue())
            jsonBody.put("Password", edtNewPassword.getValue())
            result = Networking.setParentJsonData(
                Constant.METHOD_CHANGE_PWD,
                jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }
        Networking
            .with(this)
            .getServices()
            .changePassword(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : CallbackObserver<ChangePasswordModal>() {
                override fun onSuccess(response: ChangePasswordModal) {
                    hideProgressbar()
                    if (response != null) {
                        if (response.error.equals("200")) {
                            root.showSnackBar(response.message.toString())
                            session.clearSession()
                            session.storeDataByKey(SessionManager.IsFirst, false)
                            goToActivityAndClearTask<LoginActivity>()
                        } else {
                            showAlert(response.message.toString())
                        }

                    } else {
                        showAlert(response.message.toString())
                    }
                }

                override fun onFailed(code: Int, message: String) {
                     // showAlert(message)
                    showAlert(getString(R.string.show_server_error))
                    hideProgressbar()
                }

            }).addTo(autoDisposable)
    }
}