package com.rm.enterprise.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rm.enterprise.R
import com.rm.enterprise.extention.getRandomMaterialColor
import com.rm.enterprise.extention.visible
import com.rm.enterprise.modal.ProcessListDataItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_process.*

class ProcessAdapter(
    private val mContext: Context,
    var list: MutableList<ProcessListDataItem> = mutableListOf(),
    private val listener: ProcessAdapter.OnItemSelected
) : RecyclerView.Adapter<ProcessAdapter.ItemHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.row_process,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]

        holder.bindData(mContext, data, listener)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: ProcessListDataItem)
    }

    class ItemHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bindData(
            context: Context,
            data: ProcessListDataItem,
            listener: ProcessAdapter.OnItemSelected
        ) {
            txtName.text = data.visitorName
            txtDate.text = data.createdDate
            txtDesc.text = data.discription

            imgProfile.setImageResource(R.drawable.bg_circle)
            imgProfile.setColorFilter(getRandomMaterialColor("400", context))
            txtIcon.text = data.visitorName.toString().substring(0, 1)
            txtIcon.visible()
            itemView.setOnClickListener { listener.onItemSelect(adapterPosition, data) }
        }


    }
}