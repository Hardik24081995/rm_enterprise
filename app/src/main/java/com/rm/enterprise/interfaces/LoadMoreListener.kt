package com.rm.enterprise.interfaces

/**
 * Created by HARDIK
 */

interface LoadMoreListener {
    fun onLoadMore()
}
