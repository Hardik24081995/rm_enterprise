data class TrainingSpinnerListModel(

    @field:SerializedName("data")
    val data: List<DataItem?>? = null,

    @field:SerializedName("error")
    val error: Int? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class DataItem(

    @field:SerializedName("Training")
    val training: String? = null,

    @field:SerializedName("Status")
    val status: String? = null,

    @field:SerializedName("Rno")
    val rno: String? = null,

    @field:SerializedName("rowcount")
    val rowcount: String? = null,

    @field:SerializedName("TrainingID")
    val trainingID: String? = null,

    @field:SerializedName("TrainingTime")
    val trainingTime: String? = null,

    @field:SerializedName("TrainingDateTimeID")
    val trainingDateTimeID: String? = null,

    @field:SerializedName("TrainingDate")
    val trainingDate: String? = null
)

