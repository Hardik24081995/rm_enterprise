package com.rm.enterprise.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.bumptech.glide.Glide
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.ActivityResult
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.messaging.FirebaseMessaging
import com.rm.enterprise.R
import com.rm.enterprise.extention.addFragment
import com.rm.enterprise.extention.showToast
import com.rm.enterprise.fragment.ProfileMainFragment
import com.rm.enterprise.utils.Constant
import com.rm.enterprise.utils.Constant.REQUEST_CODE_FLEXI_UPDATE
import com.rm.enterprise.utils.SessionManager
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.app_bar_main.*


class HomeActivity : AppCompatActivity(), InstallStateUpdatedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var session: SessionManager
    var txtName: TextView? = null
    var txtEmail: TextView? = null
    var profileImage: CircleImageView? = null
    lateinit var appUpdateManager: AppUpdateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        session = SessionManager(this)
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        var headerview: View = navView.getHeaderView(0)
        var nav_main: ConstraintLayout = headerview.findViewById(R.id.nav_main)
        txtName = headerview.findViewById(R.id.txtName)
        txtEmail = headerview.findViewById(R.id.txtEmail)
        profileImage = headerview.findViewById(R.id.circleImageView)
        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerOpened(drawerView: View) {
                txtName!!.setText("${session.user.data?.firstName} ${session.user.data?.lastName}")
                txtEmail!!.setText("${session.user.data?.emailID}")

                Glide.with(this@HomeActivity)
                    .load(Constant.EMP_PROFILE + session.user.data?.photoURL)
                    .apply(
                        com.bumptech.glide.request.RequestOptions().centerCrop()
                            .placeholder(com.rm.enterprise.R.drawable.ic_profile)
                    )
                    .into(profileImage!!)
            }

            override fun onDrawerClosed(drawerView: View) {

            }

            override fun onDrawerStateChanged(newState: Int) {
                val inputMethodManager =
                    getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()?.getWindowToken(), 0)
            }
        })
        nav_main.setOnClickListener {
            this.addFragment(ProfileMainFragment(), R.id.nav_host_fragment)
            drawerLayout.closeDrawers()
        }

        appUpdateManager = AppUpdateManagerFactory.create(this)
        appUpdateManager.registerListener(this)

        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                && it.updatePriority() >= 5
                && it.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {   //  check for the type of update flow you want
                requestUpdate(it)
            }
        }

        val navController = findNavController(R.id.nav_host_fragment)


        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_visitor,
                R.id.nav_setting,
                R.id.nav_invoice,
                R.id.nav_customer,
                R.id.nav_employee,
                R.id.nav_quotation,
                R.id.nav_inspection,
                R.id.nav_payment,
                R.id.nav_attendance,
                R.id.nav_site
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        if (session.roleData.data?.customer?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_customer).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.employee?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_employee).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.attendance?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_attendance).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.inspection?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_inspection).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.invoice?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_invoice).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.payment?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_payment).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.penlty?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_penalti).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.sites?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_site).setVisible(false)
            navView.invalidate()
        }
        if (session.roleData.data?.visitor?.isView.equals("0")) {
            navView.getMenu().findItem(R.id.nav_visitor).setVisible(false)
            navView.invalidate()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            // val msg = getString(R.string.msg_token_fmt, token)
            Log.d("token", token.toString())
            //  Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        })

    }


    fun requestUpdate(appUpdateInfo: AppUpdateInfo?) {
        appUpdateManager.startUpdateFlowForResult(
            appUpdateInfo!!,
            AppUpdateType.IMMEDIATE, //  HERE specify the type of update flow you want
            this,   //  the instance of an activity
            REQUEST_CODE_FLEXI_UPDATE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_FLEXI_UPDATE) {
            when (resultCode) {

                Activity.RESULT_OK -> {

                }
                Activity.RESULT_CANCELED -> {

                }
                ActivityResult.RESULT_IN_APP_UPDATE_FAILED -> {

                }

            }
        }
    }


    override fun onResume() {
        txtName!!.setText("${session.user.data?.firstName} ${session.user.data?.lastName}")
        txtEmail!!.setText("${session.user.data?.emailID}")

        Glide.with(this)
            .load(Constant.EMP_PROFILE + session.user.data?.photoURL)
            .apply(
                com.bumptech.glide.request.RequestOptions().centerCrop()
                    .placeholder(com.rm.enterprise.R.drawable.ic_profile)
            )
            .into(profileImage!!)



        appUpdateManager.appUpdateInfo.addOnSuccessListener {
            if (it.installStatus() == InstallStatus.DOWNLOADED) {
                notifyUser()
            }

        }
        super.onResume()
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }



    override fun onDestroy() {
        super.onDestroy()
        appUpdateManager.unregisterListener(this)
    }

    private fun notifyUser() {
        Snackbar.make(
            findViewById(R.id.drawer_layout),
            "An update has just been downloaded.",
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction("RESTART") { appUpdateManager.completeUpdate() }
            setActionTextColor(resources.getColor(R.color.whitePrimary))
            show()
        }
        showToast("call update")
    }

    override fun onStateUpdate(state: InstallState) {
        if (state!!.installStatus() == InstallStatus.DOWNLOADED) {
            notifyUser()
        }
    }


}