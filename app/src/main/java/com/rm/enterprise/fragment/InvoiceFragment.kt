package com.rm.enterprise.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.tabs.TabLayout
import com.rm.enterprise.R
import com.rm.enterprise.adapter.ViewPagerPagerAdapter
import com.rm.enterprise.extention.setHomeScreenTitle
import com.rm.enterprise.modal.CustomerDataItem
import kotlinx.android.synthetic.main.fragment_invoice.*

class InvoiceFragment() : BaseFragment() {

    var customerId: Int? = -1
    var visitorId: Int? = -1

    constructor(customerData: CustomerDataItem?) : this() {
        customerId = customerData?.customerID?.toInt()
        visitorId = customerData?.visitorID?.toInt()
    }

    lateinit var mParent: View

    var viewPageradapter: ViewPagerPagerAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mParent = inflater.inflate(R.layout.fragment_invoice, container, false)
        return mParent
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (customerId == -1)
            setHomeScreenTitle(requireActivity(), getString(R.string.nav_invoice))
        setStatePageAdapter()
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
                val fm = childFragmentManager
                val ft = fm.beginTransaction()
                val count = fm.backStackEntryCount
                if (count >= 1) {
                    childFragmentManager.popBackStack()
                }
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                // setAdapter();


            }

            override fun onTabReselected(tab: TabLayout.Tab) {

                //   viewPager.notifyAll();
            }
        })
    }


    private fun setStatePageAdapter() {
        viewPageradapter = ViewPagerPagerAdapter(childFragmentManager)
        viewPageradapter?.addFragment(InvoicePaidFragment(), "Paid")
        viewPageradapter?.addFragment(PartiallyPaidFragment(), "Partially Paid")
        viewPageradapter?.addFragment(InvoiceUnPaidFragment(), "Unpaid")
        viewPager.adapter = viewPageradapter
        tabs.setupWithViewPager(viewPager, true)

    }


}